package br.com.inter.digitounico.service;

import br.com.inter.digitounico.model.dto.ResultDTO;
import br.com.inter.digitounico.model.entity.Result;
import br.com.inter.digitounico.model.entity.User;
import br.com.inter.digitounico.repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class ResultService {
    @Autowired
    private ResultRepository resultRepository;

    @Autowired
    private UserService userService;

    public Result create(ResultDTO resultDTO) throws IOException, ClassNotFoundException {

        //Se existe calculo já realizado com parametros de entrada informados -> utiliza existente
        Result result = resultRepository.findByNAndAndK(resultDTO.getN(), resultDTO.getK());

        if(result == null) //Se não existe calculo com parametros de entrada -> calcula digito unico e salva resultado
            result = resultRepository.save(convertDTO(resultDTO));

        //Se usuário foi informado -> vincular resultado ao mesmo
        if(resultDTO.getUserId() != null && !resultDTO.getUserId().isEmpty()){
            User user = userService.findById(resultDTO.getUserId());

            if(user != null){
                Set<Result> userResults = user.getResults();
                userResults.add(result);
                user.setResults(userResults);
                userService.save(user);
            }
        }

        return result;
    }

    public Result convertDTO(ResultDTO resultDTO){
        Result result = new Result();
        result.setN(resultDTO.getN());
        result.setK(resultDTO.getK());
        result.setP(retornarNumeroConcatenado(resultDTO.getN(), resultDTO.getK()));
        result.setDigitoUnico(retornarDigitoUnico(result.getP()));
        return result;
    }

    public Integer retornarNumeroConcatenado(Integer n, Integer qtdeConcatenacao){
        if(qtdeConcatenacao > 1){
            StringBuilder number = new StringBuilder();
            for (int i = 0; i < qtdeConcatenacao; i++ ) {
                number.append(n);
            }

            return Integer.parseInt(number.toString());
        }else{
            return n;
        }
    }

    public Integer retornarDigitoUnico(Integer number){
        Integer digitoUnico = 0;
        for(int i = 0; i< number.toString().length(); i++){
            digitoUnico += Integer.parseInt(String.valueOf(number.toString().charAt(i))) ;
        }
        return digitoUnico;
    }

    public List<Result> last10Results(int qtde){
        return resultRepository.findLast10Results(qtde);
    }

    public List<Result> findUserResults(String userId) throws IOException, ClassNotFoundException {
        List<Result> results = new ArrayList<>();

        User user = userService.findById(userId);

        if(user != null){
            if(user.getResults() != null && user.getResults().size() > 0){
                for (Result r:user.getResults()) {
                    results.add(r);
                }
            }
        }

        return results;
    }
}
