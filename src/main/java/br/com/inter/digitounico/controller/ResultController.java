package br.com.inter.digitounico.controller;

import br.com.inter.digitounico.model.dto.ResultDTO;
import br.com.inter.digitounico.model.entity.Result;
import br.com.inter.digitounico.service.ResultService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Api(value = "Result")
@RestController
@RequestMapping(path = "/results")
public class ResultController {
    @Autowired
    private ResultService resultService;

    @PostMapping("/create")
    @ApiOperation(value = "Criação de resultado", notes = "Endpoint responsável pelo cálculo e criação de resultados de dígito único")
    public ResponseEntity<Result> create(@RequestBody ResultDTO resultDTO) throws IOException, ClassNotFoundException {
        return ResponseEntity.ok(this.resultService.create(resultDTO));
    }

    @GetMapping("/{qtdeResultados}")
    @ApiOperation(value= "Busca de resultados", notes = "Endpoint responsável por buscar os últimos resultados de dígito único - qtde enviada por parâmetro")
    public ResponseEntity<List<Result>> get(@PathVariable int qtdeResultados){
        return ResponseEntity.ok(this.resultService.last10Results(qtdeResultados));
    }

    @GetMapping("/user/{userId}")
    @ApiOperation(value= "Results client result", notes = "get results by user ")
    public ResponseEntity<List<Result>> getResultsByUserId(@PathVariable String userId) throws IOException, ClassNotFoundException {
        return ResponseEntity.ok(this.resultService.findUserResults(userId));
    }
}
