package br.com.inter.digitounico.model.entity;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    private String id;

    private String name;
    private String email;

    @ManyToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JoinTable(name = "user_result", joinColumns = {@JoinColumn(name = "result_id")}, inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private Set<Result> results = new HashSet<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Result> getResults() {
        return results;
    }

    public void setResults(Set<Result> results) {
        this.results = results;
    }
}
