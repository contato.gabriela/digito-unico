package br.com.inter.digitounico.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
@NoArgsConstructor
public class Result {
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    private String id;
    private Integer n;
    private Integer k;
    private Integer p;
    private Integer digitoUnico;
}
