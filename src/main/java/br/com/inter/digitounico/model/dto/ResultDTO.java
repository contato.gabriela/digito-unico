package br.com.inter.digitounico.model.dto;

public class ResultDTO {
    private Integer n;
    private Integer k;
    private Integer p;
    private Integer digitoUnico;
    private String userId;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public Integer getK() {
        return k;
    }

    public void setK(Integer k) {
        this.k = k;
    }

    public Integer getP() {
        return p;
    }

    public void setP(Integer p) {
        this.p = p;
    }

    public Integer getDigitoUnico() {
        return digitoUnico;
    }

    public void setDigitoUnico(Integer digitoUnico) {
        this.digitoUnico = digitoUnico;
    }

    public String getUserId() {
        return userId;
    }
}
