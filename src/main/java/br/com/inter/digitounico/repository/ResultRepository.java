package br.com.inter.digitounico.repository;

import br.com.inter.digitounico.model.entity.Result;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<Result, String> {
    Result findByNAndAndK(Integer n, Integer k);

    @Query("select u from Result u")
    List<Result> findAllResults(Pageable pageable);

    default List<Result> findLast10Results(int qtde) {
        return findAllResults(PageRequest.of(0, qtde, Sort.by("id").descending()));
    }


}
