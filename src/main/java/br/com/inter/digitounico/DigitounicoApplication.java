package br.com.inter.digitounico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class DigitounicoApplication {

	public static void main(String[] args) {
		System.out.println("Hello Word!");
		SpringApplication.run(DigitounicoApplication.class, args);
	}

}
