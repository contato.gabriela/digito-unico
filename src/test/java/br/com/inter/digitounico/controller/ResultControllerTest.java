package br.com.inter.digitounico.controller;

import br.com.inter.digitounico.model.dto.ResultDTO;
import br.com.inter.digitounico.model.entity.Result;
import br.com.inter.digitounico.model.entity.User;
import br.com.inter.digitounico.models.Entities;
import br.com.inter.digitounico.service.ResultService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ResultController.class)
class ResultControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ResultService resultService;

    @Test
    void create() throws Exception {
        ResultDTO resultDTO = Entities.getResultDTO();
        Result result = Entities.getResultEntity();

        when(resultService.create(resultDTO)).thenReturn(result);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.post("/results/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(result))).andExpect(status().isOk());
    }

    @Test
    void get() throws Exception {
        List<Result> resultList = new ArrayList<>();
        Result result = Entities.getResultEntity();
        int qtde = 10;

        resultList.add(result);

        when(resultService.last10Results(qtde)).thenReturn(resultList);

        mockMvc.perform(MockMvcRequestBuilders.get("/results/{qtdeResultados}", qtde)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

    }

    @Test
    void getResultsByUserId() throws Exception {
        Result result = Entities.getResultEntity();
        String id = "teste_id";

        List<Result> resultList = new ArrayList<>();
        resultList.add(result);

        when(resultService.findUserResults(id)).thenReturn(resultList);

        mockMvc.perform(MockMvcRequestBuilders.get("/results/user/{userId}", id)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

    }
}
