package br.com.inter.digitounico.models;

import br.com.inter.digitounico.model.dto.ResultDTO;
import br.com.inter.digitounico.model.dto.UserDTO;
import br.com.inter.digitounico.model.entity.Result;
import br.com.inter.digitounico.model.entity.User;


public class Entities {

    public static User getUser(){
        User user = new User();
        user.setId("teste_id");
        user.setName("teste_nome");
        user.setEmail("teste_email");

        return user;
    }



    public static UserDTO getUserDTO(){
        UserDTO user = new UserDTO();
        user.setName("teste_nome");
        user.setEmail("teste_email");

        return user;
    }

    public static ResultDTO getResultDTO(){
        ResultDTO resultDTO = new ResultDTO();
        resultDTO.setN(1);
        resultDTO.setK(3);
        resultDTO.setP(3);
        resultDTO.setDigitoUnico(3);
        return resultDTO;
    }

    public static Result getResultEntity(){
        Result result = new Result();
        result.setId("teste_id");
        result.setK(3);
        result.setP(3);
        result.setN(1);
        result.setDigitoUnico(3);
        return result;
    }
}
