package br.com.inter.digitounico.service;

import br.com.inter.digitounico.model.dto.UserDTO;
import br.com.inter.digitounico.model.entity.User;
import br.com.inter.digitounico.models.Entities;
import br.com.inter.digitounico.repository.UserRepository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class UserServiceTest {
    @MockBean
    UserService userService;

    @Mock
    UserRepository userRepository;

    @Test
    public void findById() throws IOException, ClassNotFoundException {
        User user = Entities.getUser();
        UserDTO userDTO = Entities.getUserDTO();

        when(userRepository.save(user)).thenReturn(user);
        when(userService.create(userDTO)).thenReturn(user);
        User response = userService.create(userDTO);

        Assert.assertEquals(user, response);
        Assert.assertTrue(response.getId().equals("teste_id"));
    }
}
